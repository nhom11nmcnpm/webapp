@extends('guest.master')
@section('title')
<title>Đổi mật khẩu</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/changepass.css">
@stop
@section('main')
<main>
		<div class="container">
			<h2 class="page-title">Bạn quên mật khẩu?</h2>
			<form class="formRegister" method="post" action="{{asset('post-forgotpass')}}" enctype="multipart/form-data">
			@include('errors.note')
				<h4>Nhập email của bạn</h4>
				<input type="email" class="form-control" name="email" placeholder="Email" required>
				<input type="submit" name="" value="Xác nhận" class="btn btn-success" style="margin-top: 30px;">
				{{csrf_field()}}
			</form>
		</div>
	</main>
@stop
