@extends('guest.master')
@section('title')
<title>Giỏ hàng</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/cart.css">
@stop
@section('main')
<main>
		<div class="container">
			<h1 style="margin-bottom:50px;">Giỏ hàng</h1>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Sản phẩm</th>
						<th>Hình ảnh</th>
						<th>Size</th>
						<th>Đơn giá</th>
						<th>Số lượng</th>
						<th>Số tiền</th>
						<th>Thao Tác</th>
					</tr>
				</thead>
				@include('errors.note')
				@if(count($cart) == 0)
				<p style="color:red; font-size:26px;">Không có sản phẩm nào trong giỏ hàng</p>
				@else
				<tbody>
					@foreach($cart as $item)
					<tr>
						<td>{{$item->name}}</td>
						<td><img src="../images/{{$item->attributes->avatar}}" style="width: 80px;height: 60px;border-radius: 3px;"></td>
						<td>{{$item->attributes->size}}</td>
						<td>{{number_format($item->price,0,",",".")}}</td>
						<td>
							{{$item->quantity}}
						</td>
						<td>{{number_format($item->price*$item->quantity,0,",",".")}}</td>
						<td><a href="{{asset('xoa-san-pham/'.$item->id)}}">xóa</a></td>
					</tr>
					@endforeach
					<tr>
						<td>
							<div class="total">Tổng tiền</div>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<div class="total">{{number_format(Cart::getTotal(),0,",",".")}} VNĐ</div>
						</td>
						<td></td>
					</tr>
					
				</tbody>
			</table>
			
			<div href="" class="pay" data-toggle="modal" @if(Auth::check()) data-target="#exampleModal" @else data-target="#exampleModal2" @endif>Thanh toán</div>
			
			<a href="{{asset('/')}}" class="continude">Tiếp tục mua hàng</a>
			@endif
		</div>
	</main>

<!-- MODAL -->
@if(Auth::check())
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 	<div class="modal-dialog" role="document">
		<div class="modal-content" style="margin-top: 50%;transform: translateY(-50%);">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Địa chỉ nhận hàng</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<form method="post" action="{{asset('thanh-toan')}}" enctype="multipart/form-data">
		<div class="modal-body">
			<p>Họ và tên:</p>
			<input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
			<p>Số điện thoại:</p>
			<input type="text" class="form-control" name="phone" value="{{Auth::user()->phone}}">
			<p>Email:</p>	
			<input type="email" class="form-control" name="email" value="{{Auth::user()->email}}">
			<p>Địa chỉ:</p>
			<input type="text" class="form-control" name="address" placeholder="Địa chỉ">
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
			<input type="submit" value="Xác nhận" class="btn btn-primary">
		
		</div>
		{{csrf_field()}}
		</form>
		</div>
  	</div>
</div>
@endif
<!--END MODAL -->
<!-- MODAL -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 	<div class="modal-dialog" role="document">
		<div class="modal-content" style="margin-top: 50%;transform: translateY(-50%);">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Chưa đăng nhập</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		
		<div class="modal-body">
				<div>Vui lòng đăng nhập để thanh toán!</div>
			</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
			<a href="{{asset('login')}}" class="btn btn-success">Đăng nhập/Đăng ký</a>
		
		</div>
		{{csrf_field()}}
		
		</div>
  	</div>
</div>
<!--END MODAL -->
@stop

