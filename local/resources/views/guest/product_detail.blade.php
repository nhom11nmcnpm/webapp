@extends('guest.master')
@section('title')
<title>Thời trang nam</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/home.css">
<link rel="stylesheet" type="text/css" href="css/product-detail.css">
<style>
.slide{
	z-index: unset;
	
}
.max_form{
	display:none;
}
</style>
@stop
@section('main')


	<main>
		<div id="content">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-12">
						
						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="carousel-item active">
									<div class="images" style="background: url('../images/{{$product->avatar}}')no-repeat center/cover;">		
									</div>
								</div>
								
								@if(count($images)>1)
									@foreach($images as $key => $item)
									<div class="carousel-item">
										<div class="images" style="background: url('../images/{{$item}}')no-repeat center/cover;">		
										</div>
									</div>
									@endforeach
								@endif
							</div>
							<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<div class="col-md-7 col-12">
					@include('errors.note')
						<div class="infor">
							<h3>{{$product->pr_name}}</h3>
							<p class="price"><u>Giá bán:</u> <span>{{number_format($product->price,0,",",".")}} VNĐ</span></p>
							<p class="status"><u>Tình trạng:</u> 
								
								@if(count($size) > 0)
								<span>Còn hàng</span>
								@else
								<span style="color:red;">Hết hàng</span>
								@endif
								
							</p>
							<div class="detail">
								<p><u>Điểm nổi bật:</u></p>
								{{$product->description}}
							</div>
							<form method="post" class="formRegister"  action="{{asset('mua-hang/'.$product->product_id)}}">
								<div class="choose">
									<div class="size">
										<p>Size*</p>
										@foreach($size as $item)
										<div class="btn btn-light btn-{{$item->size}}">{{$item->size}}
											<div class="max_form">{{$item->quantity}}</div>
										</div>
										@endforeach				
									</div>
									<div class="count">
										<p>Số lượng*</p>
										
										<input type="number" min="1" class="form_max_quantity" value="1">
										<div class="quantity"></div>
									</div>
								</div>
								
								<button type="submit" class="add-cart">
									<img src="images/cart-white.png">
									<p>Thêm vào giỏ hàng</p>
								</button>
								{{csrf_field()}}
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="list-comment">
			<div class="container">
				<div class="title">
					ĐÁNH GIÁ SẢN PHẨM
				</div>
				@foreach($comment as $item)
				<div class="comment">
					<img src="../images/{{$item->userComment->avatar}}">
					<div class="profile">
						<div class="name">{{$item->userComment->name}}</div>
						<div class="phan-hoi">{{$item->content}}</div>
						<div class="date">{{$item->created_at}}</div>
					</div>
				</div>
				@endforeach
				
				<div class="text-cmt" style="margin-bottom:30px;">
					
					@if(Auth::check())
					<form method="post" action="{{asset('comment/'.$product->product_id)}}" enctype="multipart/form-data">
						<textarea placeholder="Nhận xét của bạn!" name="comment"></textarea>
						<input type="submit">
						{{csrf_field()}}
					</form>
					@else
					<div style="color:red;font-style:italic;">Mời bạn đăng nhập để bình luận!</div>
					@endif
				</div>
				{{$comment->links()}}
			</div>
		</div>
<!-- 
		<div id="content-4">
			<div class="container">
				<div class="title">
					SẢN PHẨM LIÊN QUAN
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-12">
						<div class="product-2">
							<a href="#" class="images-2" style="background: url('images/product.png') no-repeat center/cover;">
							</a>
							<div class="infor-2">
								<p>Printed Chiffon Dress</p>
								<div class="price">
									<p>$ 61.19</p>
									<a href="">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<div class="product-2">
							<a href="#" class="images-2" style="background: url('images/product.png') no-repeat center/cover;">
							</a>
							<div class="infor-2">
								<p>Printed Chiffon Dress</p>
								<div class="price">
									<p>$ 61.19</p>
									<a href="">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<div class="product-2">
							<a href="#" class="images-2" style="background: url('images/product.png') no-repeat center/cover;">
							</a>
							<div class="infor-2">
								<p>Printed Chiffon Dress</p>
								<div class="price">
									<p>$ 61.19</p>
									<a href="">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<div class="product-2">
							<a href="#" class="images-2" style="background: url('images/product.png') no-repeat center/cover;">
							</a>
							<div class="infor-2">
								<p>Printed Chiffon Dress</p>
								<div class="price">
									<p>$ 61.19</p>
									<a href="">Add to cart</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>

		</div> -->
	</main>

@stop
@section('script')
<script type="text/javascript" src="js/quantity.js"></script>
@stop