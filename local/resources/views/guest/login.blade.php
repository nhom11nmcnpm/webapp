
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<base href="{{ asset('local/storage/app/public/guest') }}/">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300i,400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/master.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">

</head>
<body>
	<div class="login">
		<div class="loginBody">
			<div class="loginMain">
				<div class="loginMainTitle">
					<div class="loginMainTitleLeft">
						Đăng Nhập
					</div>
					<!-- <div class="loginMainTitleRight">
						<a href="{{asset('redirect/google')}}">
							<img src="images/image (2).png">
						</a>
						<a  href="{{asset('redirect/facebook')}}">
							<img src="images/image 2.png">
						</a>
					</div> -->
				</div>
				<form method="post" class="formLogin" action="{{asset('login')}}" enctype="multipart/form-data">
					<div class="" >
						<input type="text" class="inputItem" name="email" required placeholder="Email" id="emailLogin">
						<input type="password" class="inputItem" name="password" required placeholder="Mật khẩu" id="passwordLogin">
						<div id="pass_error" class="error_mess">@include('errors.note')</div>
					</div>
					
					<div class="formBot">
						<a href="{{ asset('quen-mat-khau') }}" class="forgotPass">
							Bạn quên mật khẩu
						</a>
						<div class="formSbm">
							<input type="submit" name="" class="inputSbm" value="Đăng Nhập">
						</div>
					</div>
					{{csrf_field()}}
				</form>
				<form method="post" class="formRegister"  action="{{asset('signup')}}">
					<div class="" >
						<input type="text" class="inputItem" name="name" required placeholder="Họ và Tên">
						<input type="text" class="inputItem" name="email" required placeholder="Email">
						<input type="password" class="inputItem" name="password" required placeholder="Mật khẩu" id="passwordRegister">
						<div id="pass_regis_error" class="error_mess"></div>
						<input type="password" class="inputItem" name="repassword" required placeholder="Nhập lại mật khẩu" id="repasswordRegister"> 
						<div id="re_pass_error" class="error_mess"></div>
					</div>
					
					<div class="formBot">
						
						<div class="formSbm">
							<input type="submit" name="" class="inputSbm" value="Đăng ký">
						</div>
					</div>
					{{csrf_field()}}
				</form>
			</div>
			
		</div>
		<div class="loginItem">
			<div class="loginItemMain">
				<div class="loginTitle">
					CHÀO MỪNG BẠN ĐẾN VỚI FASHION STORE
				</div>
				<div class="loginTitleSmall">
					thương hiệu thời trang uy tín hàng đầu Việt Nam
				</div>
				<div class="btnLogin">
					Đăng Nhập
				</div>
			</div>	
		</div>
		<div class="loginItem">
			<div class="loginItemMain">
				<div class="loginTitle">
					Đăng ký để trở thành khách hàng của FASHION STORE
				</div>
				<div class="loginTitleSmall">
					Nếu bạn chưa có tài khoản?
				</div>
				<div class="btnRegister">
					Đăng ký
				</div>
			</div>
				
		</div> 
	</div>
		
	<script type="text/javascript" src="js/bootstrap/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
	<script type="text/javascript" src="js/login.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			@if (Request::segment(1) == 'register')
				$('.btnRegister').click();
			@endif
		});
			
	</script>
</body>
</html>