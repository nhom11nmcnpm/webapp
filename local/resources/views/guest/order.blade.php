@extends('guest.master')
@section('title')
<title>Lịch sử mua hàng</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/man.css">
@stop
@section('main')
	
	<main style="margin-top:50px;">
		<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container">	
			<h3 class="page-title" style="margin-bottom:30px;">Lịch sử mua hàng</h3>
			<table class="table table-bordered" data-toggle="table" data-search="true">
				<thead>
					<tr>
						<th>Mã đơn hàng</th>						
						<th>Ngày đặt hàng</th>
						<th>Địa chỉ</th>
                        <th>Số điện thoại</th>
						<th>Trang Thái</th>
						<th>Chi tiết</th>
						<th>Hủy đơn hàng</th>
					</tr>
				</thead>
				<tbody>
					@foreach($order as $item)
					<tr>
						<td>{{$item->order_id}}</td>
					
						<td>
							{{$item->created_at}}
						</td>
						<td>{{$item->address}}</td>
                        <td>{{$item->phone}}</td>
						<td style="color:red;">{{$item->status}} 
							
						</td>
						<td>
							<a href="{{asset('chi-tiet-don-hang/'.$item->order_id)}}" class="btn btn-info">Chi tiết</a>
						</td>
						
						<td>
                            @if($item->status != 'Đã thanh toán' && $item->status != 'Đã hủy!')
							<a onclick="return Xoa();" href="{{asset('huy-don-hang/'.$item->order_id)}}" class="btn btn-danger"><i class="fa fa-trash-o"></i>Hủy</a>
                            @endif
						</td>
					</tr>
					@endforeach
					
					
				</tbody>
			</table>
		</div>
	</div>
	<!-- END MAIN CONTENT -->	
	</main>
@stop
@section('script')
<script>
function Xoa(){
    var conf= confirm("Bạn có chắc chắn muốn hủy đơn hàng không?");
    return conf;
}
</script>	

@stop
