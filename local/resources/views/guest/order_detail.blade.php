@extends('guest.master')
@section('main')
<div class="main" style="margin:70px 0;">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container">
			
			<h3 class="page-title" style="margin-bottom:30px;">Chi tiết đơn hàng</h3>
			<table class="table table-bordered" data-toggle="table">
				<thead>
					<tr>
						<th>Mã đơn hàng</th>				
                        <th>Ngày đặt hàng</th>
                        <th>Địa chỉ</th>
                        <th>Số điện thoại</th>
						<th>Trang Thái</th>	
					</tr>
				</thead>
				<tbody>		
					<tr>
						<td>{{$order->order_id}}</td>
						<td>
							{{$order->created_at}}
                        </td>
                        <td>{{$order->address}}</td>
                        <td>{{$order->phone}}</td>
						<td style="color:red;">{{$order->status}}</td>
					</tr>	
				</tbody>
            </table>
            <table class="table table-bordered" data-toggle="table" style="margin-top:50px;">
				<thead>
					<tr>
						<th>Sản phẩm</th>
						<th>Hình ảnh</th>
						<th>size</th>
                        <th>Đơn giá</th>	
                        <th>Số lượng</th>
						<th>Số tiền</th>
					</tr>
				</thead>
				<tbody>		
                    @foreach($order_detail as $item)
					<tr>
						<td>{{$item->oderProduct->pr_name}}</td>
						<td><img src="../images/{{$item->oderProduct->avatar}}" width="80px" height="60px" style="border-radius: 5px;"></td>
						<td>
							{{$item->size}}
						</td>
                        <td>{{number_format($item->oderProduct->price,0,",",".")}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{number_format($item->oderProduct->price*$item->quantity,0,",",".")}}</td>
                    </tr>	
                    @endforeach
                    <tr>
						<td>
							<div class="total">Tổng tiền</div>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<div class="total">{{number_format($total_money,0,",",".")}} VNĐ</div>
						</td>
						
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop