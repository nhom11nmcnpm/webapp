@extends('guest.master')
@section('title')
<title>Tìm kiếm</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/shop.css">

@stop
@section('main')
<main>
		
		<div class="container">
			<div class="sort-bar">
				<ul>
					<li class="sort-bar-label">Sắp xếp theo:</li>
					<li><a href="{{asset('newbie/'.$search)}}" class="opption1">Mới nhất</a></li>
					<li class="opption2">Giá: {{$text}}
						<ul class="select">
							<li><a href="{{asset('downto/'.$search)}}">Cao đến thấp</a></li>
							<li><a href="{{asset('upto/'.$search)}}">Thấp đến cao</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

		<div id="content-4">
				
				<div class="container">
					<div class="row">
						@if(count($product) == 0)
						Không tìm thấy sản phẩm nào!
						@endif
						@foreach($product as $item)
						<div class="col-md-3 col-12">
							<div class="product-2">
								<a href="{{asset('chi-tiet/'.$item->slug)}}" class="images-2" style="background: url('../images/{{$item->avatar}}') no-repeat center/cover;">
								</a>
								<div class="infor-2">
									<p>{{$item->pr_name}}</p>
									<div class="price">
										<p>{{number_format($item->price,0,",",".")}} VNĐ</p>
										<a href="{{asset('chi-tiet/'.$item->slug)}}">Add to cart</a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					
					<!-- <div class="bg-3"></div> -->
				</div>

			</div>
	</main>
@stop
