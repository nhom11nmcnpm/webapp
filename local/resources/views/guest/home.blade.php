@extends('guest.master')
@section('title')
<title>Trang chủ</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/home.css">
@stop
@section('main')
<main>
		<section id="slide-show">
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					@foreach($banner as $key => $item)
				    <div class="carousel-item @if($key==0) active @endif">
				    	  <div class="slide" style="background: url('../images/{{$item->images}}') no-repeat center/cover;">
						  	<div class="container">

				    	  		<div class="text" style="margin-left:100px;">
				    	  			<h1>{{$item->title}}</h1>
									<div class="description">{{$item->description}}</div>
				    	  		</div>
							</div>
				    	  </div>
				    </div>
					@endforeach
				</div>
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					</a>
			</div>
		</section>

		<section id="main">
			

			<div id="content-2">
				<div class="container">
					<div class="title">
							THỜI TRANG NAM
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-12">
							<div class="banner-man" style="background: url('images/baner-man.jpg') no-repeat center/cover;">
								<div class="button-1">
									<p>for men</p>
									<a href="{{asset('thoi-trang-nam')}}">
										More
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-12">
							<div class="row">
								@foreach($man as $item)
								<div class="col-md-6 col-12">
									<div class="product-2">
										<a href="{{asset('chi-tiet/'.$item->slug)}}" class="images-2" style="background: url('../images/{{$item->avatar}}') no-repeat center/cover;">
										</a>
										<div class="infor-2">
											<p>{{$item->pr_name}}</p>
											<div class="price">
												<p>{{number_format($item->price,0,",",".")}} VNĐ</p>
												<a href="{{asset('chi-tiet/'.$item->slug)}}">Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="content-3">
				<div class="container">
					<div class="title">
							THỜI TRANG NỮ
					</div>
				</div>
				<div class="container">
					<div class="row">

						<div class="col-md-6 col-12">
							<div class="row">
								@foreach($woman as $item)
								<div class="col-md-6 col-12">
									<div class="product-2">
										<a href="{{asset('chi-tiet/'.$item->slug)}}" class="images-2" style="background: url('../images/{{$item->avatar}}') no-repeat center/cover;">
										</a>
										<div class="infor-2">
											<p>{{$item->pr_name}}</p>
											<div class="price">
												<p>{{number_format($item->price,0,",",".")}} VNĐ</p>
												<a href="{{asset('chi-tiet/'.$item->slug)}}">Add to cart</a>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
						<div class="col-md-6 col-12">
							<div class="banner-man" style="background: url('images/banner-girls.jpg') no-repeat center/cover;">
								<div class="button-1">
									<p>for women</p>
									<a href="{{asset('thoi-trang-nu')}}">
										More
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div id="content-4">
				<div class="container">
					<div class="title">
							SẢN PHẨM NỔI BẬT
					</div>
				</div>
				<div class="container">
					<div class="row">
						@foreach($new_pr as $item)
						<div class="col-md-3 col-12">
							<div class="product-2">
								<a href="{{asset('chi-tiet/'.$item->slug)}}" class="images-2" style="background: url('../images/{{$item->avatar}}') no-repeat center/cover;">
								</a>
								<div class="infor-2">
									<p>{{$item->pr_name}}</p>
									<div class="price">
										<p>{{number_format($item->price,0,",",".")}} VNĐ</p>
										<a href="{{asset('chi-tiet/'.$item->slug)}}">Add to cart</a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					<!-- <div class="bg-3"></div> -->
				</div>

			</div>
		</section>
	</main>
@stop
