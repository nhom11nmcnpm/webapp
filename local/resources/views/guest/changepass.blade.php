@extends('guest.master')
@section('title')
<title>Đổi mật khẩu</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/changepass.css">
@stop
@section('main')
<main>
		<div class="container">
			<h2 class="page-title">Đổi mật khẩu</h2>
			<form class="formRegister" method="post" action="{{asset('post-changepass/'.Auth::user()->id)}}" enctype="multipart/form-data">
			@include('errors.note')
				<h4>Mật khẩu cũ</h4>
				<input type="password" class="form-control" name="password_old" placeholder="Mật khẩu cũ">


				<h4>Mật khẩu mới</h4>
				<input type="password" class="inputItem form-control" name="password" required placeholder="Mật khẩu mới" id="passwordRegister">
				<div id="pass_regis_error" class="error_mess"></div>

				<h4>Nhập lại mật khẩu</h4>
				<input type="password" class="inputItem form-control" name="repassword" required placeholder="Nhập lại mật khẩu" id="repasswordRegister"> 
				<div id="re_pass_error" class="error_mess" style="color:red;"></div>
						
				<input type="submit" name="" value="Xác nhận" class="btn btn-success" style="margin-top: 30px;">
				{{csrf_field()}}
			</form>
		</div>
	</main>
@stop
@section('script')
<script type="text/javascript">
	function changeImg(input){
         //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
         if(input.files && input.files[0]){
         	var reader = new FileReader();
             //Sự kiện file đã được load vào website
             reader.onload = function(e){
                 //Thay đổi đường dẫn ảnh
                 $('.avatar').attr('src',e.target.result);
             }
             reader.readAsDataURL(input.files[0]);
         }
     }
     $(document).ready(function() {
     	$('.avatar').click(function(){
     		$('.img').click();
     	});         
	 });
	 $('.formRegister').submit(function(){
		var flag = true;
		var passwordRegister    = $.trim($('#passwordRegister').val());
		var repasswordRegister    = $.trim($('#repasswordRegister').val());
		if (passwordRegister.length <= 0){
			$('#pass_regis_error').text('Bạn chưa nhập mật khẩu');
			flag = false;
		}
		else{
			$('#pass_regis_error').text('');
		}

		if (passwordRegister != repasswordRegister){
			$('#re_pass_error').text('Nhập lại mật khẩu không trùng khớp');
			flag = false;
		}
		else{
			$('#re_pass_error').text('');
		}
		localStorage.setItem('sp',$('#passwordLogin').val());
		
		return flag;
	});
 </script>
 @stop