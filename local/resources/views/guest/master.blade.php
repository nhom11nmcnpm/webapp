<!DOCTYPE html>
<html>
<head>

	@yield('title')

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<base href="{{ asset('local/storage/app/public/guest') }}/">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300i,400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/master.css">

	@yield('css')
</head>
<body>

	<header id="header">
		<div id="header-top">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="top top-active">
							<img src="images/cart.png">
							Free shipping ON all order
						</div>
					</div>
					<div class="col-md-4">
						<div class="top top-active">
							<img src="images/dolar.png">
							100% MONEY BACK GUARANTEE
						</div>
					</div>
					<div class="col-md-4">
						<div class="top">
							<img src="images/time.png">
							ONLINE SUPPORT 24/7
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="header-mid">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-12">
						<a href="{{asset('/')}}" class="logo">
							<img src="images/logo.png">
						</a>
					</div>
					<div class="col-md-6 col-12">
						
						<form class="form-search" method="post" action="{{asset('search')}}" enctype="multipart/form-data">
							<input type="text" name="search" placeholder="Nhập từ khóa tìm kiếm">
							<input type="submit" name="" value="tìm">
							{{csrf_field()}}
						</form>
						
					</div>
					<div class="col-md-3 col-12">
						
						@if(Auth::check())
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span>{{Auth::user()->name}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i><img src="../images/{{Auth::user()->avatar}}" class="img-circle" alt="Avatar"></a>
							<ul class="dropdown-menu">
								<li><a href="{{asset('profile/'.Auth::user()->id)}}"><i class="far fa-user-alt"></i> <span>My Profile</span></a></li>
								<li><a href="{{asset('doi-mat-khau')}}"><i class="far fa-key"></i> <span>Đổi mật khẩu</span></a></li>
								<li><a href="{{asset('lich-su-mua-hang/'.Auth::user()->id)}}"><i class="far fa-key"></i> <span>Lịch sử mua hàng</span></a></li>
								<li><a href="{{asset('logout')}}"><i class="far fa-sign-out-alt"></i> <span>Logout</span></a></li>
							</ul>
						@else
						<a href="{{asset('login')}}" class="login">
							Đăng kí | Đăng nhập
						</a>
						@endif
						<div class="contact">
							<a href="{{asset('https://www.facebook.com/conbo.son')}}"><img src="images/fb.png"></a>
							<a href="{{asset('https://www.facebook.com/conbo.son')}}"><img src="images/insta.png"></a>
							<a href="{{asset('https://www.facebook.com/conbo.son')}}"><img src="images/yt.png"></a>
							<a href="{{asset('https://www.facebook.com/conbo.son')}}"><img src="images/switer.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="header-bt">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-12">
						<div>
							<ul class="nav">
						    <li><a href="{{asset('/')}}">Trang chủ</a></li>
								<li><a href="{{asset('cua-hang')}}">Cửa hàng</a></li>
						    <li>
						        <a href="{{asset('thoi-trang-nam')}}">Thời trang nam</a>
						        <ul class="sub-menu">
									@foreach($man_category as $item)
						            <a href="{{asset('thoi-trang-nam/'.$item->name)}}"><li>{{$item->name}}</li></a>
									@endforeach
						            
						        </ul>
						    </li>
						    <li><a href="{{asset('thoi-trang-nu')}}">Thời trang nữ</a>
									<ul class="sub-menu">
									@foreach($woman_category as $item)
						            <a href="{{asset('thoi-trang-nu/'.$item->name)}}"><li>{{$item->name}}</li></a>
									@endforeach
									</ul>
								</li>
						    <li><a href="{{asset('gioi-thieu')}}">Giới thiệu</a></li>
						</ul>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<a href="{{asset('gio-hang')}}" class="cart">
							<img src="images/cart-white.png">
							<p>Shopping Cart ({{Cart::getTotalQuantity()}})</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>

	@yield('main')

	<footer>
		<div id="footer-top">
			<div class="container border-1">
				<div class="row">
					<div class="col-md-3 col-12">
						<div class="logo">
							<img src="images/logo.png">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							</p>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<div class="contact-ft">
							<h3>Contact</h3>
							<p><i class="fas fa-map-marker-alt"></i> address: Lorem ipsum dolor sit amet,nsectetuer adipiscing elit</p>
							<p><i class="far fa-envelope"></i> infor@gmail.com</p>
							<p><i class="fas fa-phone"></i> 0126889226965</p>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<div class="contact-ft">
							<h3>Contact</h3>
							<p><i class="fas fa-map-marker-alt"></i> address: Lorem ipsum dolor sit amet,nsectetuer adipiscing elit</p>
							<p><i class="far fa-envelope"></i> infor@gmail.com</p>
							<p><i class="fas fa-phone"></i> 0126889226965</p>
						</div>
					</div>
					<div class="col-md-3 col-12">
						<div class="contact-ft">
							<h3>Contact</h3>
							<p><i class="fas fa-map-marker-alt"></i> address: Lorem ipsum dolor sit amet,nsectetuer adipiscing elit</p>
							<p><i class="far fa-envelope"></i> infor@gmail.com</p>
							<p><i class="fas fa-phone"></i> 0126889226965</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer-bt">
			© 2018 copyright. bkhn
		</div>
	</footer>

	

	<script type="text/javascript" src="js/bootstrap/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dropdown-toggle').click(function() {
 			 	$('.dropdown-menu').slideToggle();
			});
		});
	</script>
	@yield('script')
</body>
</html>
