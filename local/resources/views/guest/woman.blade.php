@extends('guest.master')
@section('title')
<title>Thời trang nữ</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/woman.css">
@stop
@section('main')
<main>
		<div id="main-top">
			<div class="container" style="position: relative;">
				<div class="title-top">
					<h2>WOMAN</h2>
					<p>Thời trang nữ, phong cách 2018 the fashion shop</p>
				</div>
			</div>
		</div>

		<div id="content-4">
				<div class="container">
					<div class="title">
						WOMAN
					</div>
				</div>
				<div class="container">
					<div class="row">
					@foreach($product as $item)
						<div class="col-md-3 col-12">
							<div class="product-2">
								<a href="{{asset('chi-tiet/'.$item->slug)}}" class="images-2" style="background: url('../images/{{$item->avatar}}') no-repeat center/cover;">
								</a>
								<div class="infor-2">
									<p>{{$item->pr_name}}</p>
									<div class="price">
										<p>{{number_format($item->price,0,",",".")}} VNĐ</p>
										<a href="{{asset('chi-tiet/'.$item->slug)}}">Add to cart</a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					{{$product->links()}}
					<!-- <div class="bg-3"></div> -->
				</div>

			</div>
	</main>
@stop
