@extends('guest.master')
@section('title')
<title>Thông tin cá nhân</title>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="css/profile.css">
@stop
@section('main')
<main>
		<div class="container">
			<h2 class="page-title" style="font-weight: bold;">Thông tin tài khoản</h2>
			<form class="formRegister" method="post" action="{{asset('post-profile/'.Auth::user()->id)}}" enctype="multipart/form-data">
			@include('errors.note')
				<div class="row">

					<div class="col-md-6 col-12">
						<h4>Họ và tên</h4>
						<input type="text" class="form-control" name="name" value="{{$user->name}}">

						<h4>Email</h4>
						<p class="form-control">{{$user->email}}</p>

						<h4>Số điện thoại</h4>
						<input type="text" class="form-control" name="phone" value="{{$user->phone}}">
						
					</div>
					<div class="col-md-6 col-12">
						<h4>Avatar</h4>
						<input class="img" type="file" name="avatar" class="cssInput " onchange="changeImg(this)" style="display: none;!important;">
						<img style="cursor: pointer;" class="avatar" class="cssInput thumbnail tableImgAvatar" width="300" height="350px" src="../images/{{$user->avatar}}">
					</div>
				</div>

				<input type="submit" name="" value="Chỉnh sửa" class="btn btn-success" style="margin-top: 30px;">
				{{csrf_field()}}
			</form>
		</div>
	
	</main>
@stop
@section('script')
<script type="text/javascript">
	function changeImg(input){
         //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
         if(input.files && input.files[0]){
         	var reader = new FileReader();
             //Sự kiện file đã được load vào website
             reader.onload = function(e){
                 //Thay đổi đường dẫn ảnh
                 $('.avatar').attr('src',e.target.result);
             }
             reader.readAsDataURL(input.files[0]);
         }
     }
     $(document).ready(function() {
     	$('.avatar').click(function(){
     		$('.img').click();
     	});         
	 });
	 
 </script>
 @stop