@extends('admin.master')
@section('main')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h2 class="page-title" style="font-weight: bold;">Thêm tài khoản</h2>
			<form class="formRegister" method="post" action="{{asset('admin/user/postadd')}}" enctype="multipart/form-data">
				<div class="row">

					<div class="col-md-6 col-12">
						<h4>Họ và tên</h4>
						<input type="text" class="form-control" name="name" placeholder="Họ và tên">

						<h4>Email</h4>
						<input type="email" class="form-control" name="email" placeholder="Email">

						<h4>Số điện thoại</h4>
						<input type="text" class="form-control" name="phone" placeholder="Số điện thoại">

						<h4>Mật khẩu</h4>
						<input type="password" class="inputItem form-control" name="password" required placeholder="Mật khẩu" id="passwordRegister">
						<div id="pass_regis_error" class="error_mess"></div>

						<h4>Nhập lại mật khẩu</h4>
						<input type="password" class="inputItem form-control" name="repassword" required placeholder="Nhập lại mật khẩu" id="repasswordRegister"> 
						<div id="re_pass_error" class="error_mess" style="color:red;"></div>
						<h4>Chức vụ</h4>
						<select class="form-control" name="level">
							<option value="3">Quản trị viên</option>
							<option value="2">Nhân viên</option>
						</select>
						
					</div>
					<div class="col-md-6 col-12">
						<h4>Avatar</h4>
						<input class="img" type="file" name="avatar" class="cssInput " onchange="changeImg(this)" style="display: none;!important;">
						<img style="cursor: pointer;" class="avatar" class="cssInput thumbnail tableImgAvatar" width="300" height="350px" src="../images/default-avatar.jpeg">
					</div>
				</div>

				<input type="submit" name="" value="Thêm mới" class="btn btn-success" style="margin-top: 30px;">
				{{csrf_field()}}
			</form>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop
@section('script')
<script type="text/javascript">
	function changeImg(input){
         //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
         if(input.files && input.files[0]){
         	var reader = new FileReader();
             //Sự kiện file đã được load vào website
             reader.onload = function(e){
                 //Thay đổi đường dẫn ảnh
                 $('.avatar').attr('src',e.target.result);
             }
             reader.readAsDataURL(input.files[0]);
         }
     }
     $(document).ready(function() {
     	$('.avatar').click(function(){
     		$('.img').click();
     	});         
	 });
	 $('.formRegister').submit(function(){
		var flag = true;
		var passwordRegister    = $.trim($('#passwordRegister').val());
		var repasswordRegister    = $.trim($('#repasswordRegister').val());
		if (passwordRegister.length <= 0){
			$('#pass_regis_error').text('Bạn chưa nhập mật khẩu');
			flag = false;
		}
		else{
			$('#pass_regis_error').text('');
		}

		if (passwordRegister != repasswordRegister){
			$('#re_pass_error').text('Nhập lại mật khẩu không trùng khớp');
			flag = false;
		}
		else{
			$('#re_pass_error').text('');
		}
		localStorage.setItem('sp',$('#passwordLogin').val());
		
		return flag;
	});
 </script>
 @stop