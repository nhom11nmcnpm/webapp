@extends('admin.master')
@section('main')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Quản lý người dùng</h3>
			<a href="{{asset('admin/user/add')}}" class="btn btn-success">Thêm tài khoản</a>
			
			<table class="table table-bordered" data-toggle="table" data-search="true">
				<thead>
					<tr>
						<th>Họ tên</th>
						<th>Hình ảnh</th>
						<th>Chức vụ</th>
						<th>Email</th>
						<th>Số điện thoại</th>
						<th>Khóa tài khoản</th>
					</tr>
				</thead>
				<tbody>
					@foreach($user as $item)
					<tr>
						<td>{{$item->name}}</td>
						<td>
							<img src="../images/{{$item->avatar}}" width="100px" height="70px" style="border-radius: 5px;">
						</td>
						<td>
							@if($item->level == 1)
								Người dùng
							@elseif($item->level == 2)
								Nhân viên
							@else
								Quản trị viên
							@endif
						</td>
						<td>{{$item->email}}</td>
						<td>{{$item->phone}}</td>
						<td>
							@if($item->flag == 0)
							<a href="{{asset('admin/user/block/'.$item->id)}}" class="btn btn-danger"> Block</a>
							@else
							<a href="{{asset('admin/user/unblock/'.$item->id)}}" class="btn btn-success"> Unblock</a>
							@endif
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop