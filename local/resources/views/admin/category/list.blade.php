@extends('admin.master')
@section('main')
<div class="main">
	<style type="text/css">
		.value_form_tile,.action_form_tile{
			display: none;
		}
	</style>
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Phân loại sản phẩm</h3>
			<div class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Thêm</div>
			
			<table class="table table-bordered" data-toggle="table" data-search="true">
				<thead>
					<tr>
						<th>Danh mục</th>
						<th>sửa</th>
						<th>xóa</th>
					</tr>
				</thead>
				<tbody>
					@foreach($category as $key => $item)
					@if($key % 2 == 0)
					<tr>
						<td>{{$item->name}}</td>
						
						<td>
							<div class="btn btn-info edit-title" data-toggle="modal">
								Sửa
								<div class="value_form_tile">{{$item->name}}</div>
								<div class="action_form_tile">{{asset('admin/category/edit/'.$item->name)}}</div>
							</div>
						</td>
						<td>
							<a onclick="return Xoa();" href="{{asset('admin/category/delete/'.$item->name)}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Xóa</a>
						</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>

	
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 50%;transform: translateY(-50%);">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thêm danh mục</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
      <form method="post" action="{{asset('admin/category/postadd')}}" enctype="multipart/form-data">
      <div class="modal-body">
        	<input type="text" class="form-control" name="name">
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <input type="submit" value="Thêm" class="btn btn-primary">
     
      </div>
      {{csrf_field()}}
      </form>
    </div>
  </div>
</div>
<!--End Modal -->
<!-- Modal -->
<div class="modal fade" id="editmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 50%;transform: translateY(-50%);">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sửa danh mục</h5>
        
   	  </div>
      <form method="post" class="form_update_action" enctype="multipart/form-data">
      <div class="modal-body">
        	<input type="text" class="form-control form_update_tile" name="name">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        <input type="submit" value="Chỉnh sửa" class="btn btn-primary">
      </div>
      {{csrf_field()}}
      </form>
    </div>
  </div>
</div>
<!--End Modal -->
@stop
@section('script')
<script>
	$( document ).ready(function() {
		$('.edit-title').click(function(){
			console.log( "ready!" );
			var title = $(this).find('.value_form_tile').text();
			var action = $(this).find('.action_form_tile').text();

			$('.form_update_action').attr('action',action);
			$('.form_update_tile').attr('value',title);
			$('#editmodal').modal();
		});
	});
</script>

@stop