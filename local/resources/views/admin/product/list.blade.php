@extends('admin.master')
@section('main')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Quản lý sản phẩm: 
				<span style="font-weight: bold;">
				@if($category->sex == 1)
					Thời trang nam/{{$category->name}}
				@else
					Thời trang nữ/{{$category->name}}
				@endif
				<span>
			</h3>
			<a href="{{asset('admin/product/add/'.$category->category_id)}}" class="btn btn-success">Thêm sản phẩm</a>
			
			<table class="table table-bordered" data-toggle="table" data-search="true">
				<thead>
					<tr>
						<th>Tên sản phẩm</th>
						<th>Hình ảnh</th>
						<th>Số lượng</th>
						<th>giá</th>
						<th>Mô tả</th>
						<th>sửa</th>
						<th>xóa</th>
					</tr>
				</thead>
				<tbody>
				@foreach($product as $item)
					<tr>
						<td>{{$item->pr_name}}</td>
						<td>
							<img src="../images/{{$item->avatar}}" width="170px" height="120px" style="border-radius: 5px;">
						</td>					
						<td>	
							<?php
								$quantity = 0;
								foreach($item->getSize as $value){
									$quantity += $value->quantity;
								}
								echo $quantity;
							?>	
						</td>
						<td>{{$item->price}}</td>
						<td>{{$item->description}}</td>
						<td>
							<a href="{{asset('admin/product/edit/'.$item->product_id)}}" class="btn btn-info">Sửa</a>
						</td>
						<td>
							<a onclick="return Xoa();" href="{{asset('admin/product/delete/'.$item->product_id)}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Xóa</a>
						</td>
					</tr>
				@endforeach	
				</tbody>
			</table>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop