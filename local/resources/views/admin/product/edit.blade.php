@extends('admin.master')
@section('main')
<style>
.hidden{
	display:none;
}
</style>
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h2 class="page-title" style="font-weight: bold;">Sửa sản phẩm</h2>
			<form method="post" action="{{asset('admin/product/postedit/'.$product->product_id)}}" enctype="multipart/form-data">
				<div class="row">

					<div class="col-md-6 col-12">
						<h3>Tên sản phẩm</h3>
						<input type="text" class="form-control" name="pr_name" value = "{{$product->pr_name}}" required="">
						
						<h3>Giá nhập vào</h3>	
						<input type="text" class="form-control" name="im_price" required="" value = "{{$product->import_price}}">

						<h3>Giá bán ra</h3>
						
						<input type="text" class="form-control" name="price" required="" value = "{{$product->price}}">
						<h3>số lượng</h3>
						
						@for($i=36;$i < 44;$i++)

							<div class="hidden">{{$quantity=0}}</div>
							<div style="display:flex;width:20%;margin-top:10px;">
								@foreach($size as $item)
									@if($item->size == $i)
									<div class="hidden">{{$quantity=$item->quantity}}</div>
									@endif
								@endforeach
								
								<div style="flex:2;line-height:35px;">Size {{$i}}: </div>
								<input style="flex:1;width:30px;" type="text" class="form-control" name="size[]" @if($quantity != 0) value="{{$quantity}} @endif">
								
							</div>
							
						@endfor
						
						
						
						<h3>Mô tả</h3>
						
						<textarea class="form-control" style="height: 200px;" name="description" required="">{{$product->description}}</textarea>
					</div>
					<div class="col-md-6 col-12">
						<h3>Avatar</h3>
						<input class="img" type="file" name="avatar" class="cssInput " onchange="changeImg(this)" style="display: none;!important;">
						<img style="cursor: pointer;" class="avatar" class="cssInput thumbnail tableImgAvatar" width="350" height="400px" src="../images/{{$product->avatar}}">
					</div>
				</div>

				<input type="submit" name="" value="Cập nhật" class="btn btn-success" style="margin-top: 30px;">
				{{csrf_field()}}
			</form>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop
@section('script')
<script type="text/javascript">
	function changeImg(input){
         //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
         if(input.files && input.files[0]){
         	var reader = new FileReader();
             //Sự kiện file đã được load vào website
             reader.onload = function(e){
                 //Thay đổi đường dẫn ảnh
                 $('.avatar').attr('src',e.target.result);
             }
             reader.readAsDataURL(input.files[0]);
         }
     }
     $(document).ready(function() {
     	$('.avatar').click(function(){
     		$('.img').click();
     	});         
     });
 </script>
 @stop