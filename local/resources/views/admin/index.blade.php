@extends('admin.master')
@section('main')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<!-- OVERVIEW -->
			<div class="panel panel-headline">
				<div class="panel-heading">
					<h3 class="panel-title" style='font-weight:bold;font-size:28px;'>Thống kê</h3>
					<p class="panel-subtitle">Ngày: {{$date}}</p>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="fa fa-credit-card"></i></span>
								<p>
									<span class="number">{{$total_product}}</span>
									<span class="title">Mặt hàng</span>
								</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="lnr lnr-user"></i></span>
								<p>
									<span class="number">{{$total_user}}</span>
									<span class="title">Khách hàng</span>
								</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="fa fa-shopping-cart"></i></span>
								<p>
									<span class="number">{{$total_order}}</span>
									<span class="title">Đơn hàng mới</span>
								</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="metric">
								<span class="icon"><i class="fa fa-bar-chart"></i></span>
								<p>
									<span class="number">{{number_format($revenue,0,",",".")}} VNĐ</span>
									<span class="title">Tổng doanh thu</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END OVERVIEW -->
			<div class="row">
			
					<!-- RECENT PURCHASES -->
					<div class="panel">
						<div class="panel-heading" style='overflow:hidden;'>
							<h3 class="panel-title" style='font-weight:bold;font-size:24px;float:left;margin-right:10px;'>Thống kê kinh doanh năm </h3>
							<form style='width:100px;float:left;' method="post" action="{{asset('admin')}}" enctype="multipart/form-data">
								<input type="text" class='form-control' value='{{$year}}' name='year'>
								{{csrf_field()}}
							</form>
							<div class="right">
								<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
							
							</div>
						</div>
						
						<div class="panel-body no-padding">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Tháng</th>
										<th>Số đơn hàng</th>
										<th>Số sản phẩn bán được</th>
										<th>Doanh thu</th>
										<th>Lãi suất</th>
									</tr>
								</thead>
								<tbody>
									@foreach($thong_ke as $key => $item)
									<tr>
										<td>Tháng {{$key+1}}</td>
										<td>{{$item['so_don_hang']}}</td>
										<td>{{$item['so_san_pham']}}</td>
										<td>{{number_format($item['doanh_thu'],0,",",".")}} VNĐ</td>
										<td>{{number_format($item['lai_suat'],0,",",".")}} VNĐ</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						
					</div>
					<!-- END RECENT PURCHASES -->
				
				
			</div>
		
			
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop