@extends('admin.master')
@section('main')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			
			<h3 class="page-title">Danh sách đơn hàng</h3>
			<table class="table table-bordered" data-toggle="table" data-search="true">
				<thead>
					<tr>
						<th>Mã đơn hàng</th>
						<th>Khách Hàng/ID</th>
						<th>Ngày đặt hàng</th>
						<th>Địa chỉ</th>
						<th>Số điện thoại</th>
						<th>Số tiền</th>
						<th>Trang Thái</th>
						<th>Chi tiết</th>
						<th>Xóa</th>	
					</tr>
				</thead>
				<tbody>
					@foreach($order as $item)
					<tr>
						<td>{{$item->order_id}}</td>
						<td>{{$item->userOder->name}}/{{$item->userOder->id}}</td>
						<td>
							{{$item->created_at}}
						</td>
						<td>{{$item->address}}</td>
						<td>{{$item->phone}}</td>
						<td>{{number_format($item->pay,0,",",".")}} vnđ</td>
						<td>{{$item->status}} 
							@if($item->status != 'Đã thanh toán' && $item->status != 'Đã hủy!')
								@if($item->status != 'Đang giao hàng')
								<a href="{{asset('admin/order/giao-hang/'.$item->order_id)}}" class="btn btn-success" style="margin-left:10px;">Giao hàng</a>
								@endif
							<a href="{{asset('admin/order/status/'.$item->order_id)}}" class="btn btn-success" style="margin-left:10px;">Done</a>
							@endif
						</td>
						<td>
							<a href="{{asset('admin/order/order-detail/'.$item->order_id)}}" class="btn btn-info">Chi tiết</a>
						</td>
						
						<td>
							<a onclick="return Xoa();" href="{{asset('admin/order/delete/'.$item->order_id)}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Xóa</a>
						</td>
					</tr>
					@endforeach
					
					
				</tbody>
			</table>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
@stop