<?php

Route::get('test','TestController@getTest');

Route::group(['namespace' => 'guest'], function() {

    // HOME
    Route::get('/','HomeController@getHome');

    // tìm kiếm
    Route::post('search','SearchController@search');
    Route::get('newbie/{search}','SearchController@searchNewbie');
    Route::get('upto/{search}','SearchController@sortUpto');
    Route::get('downto/{search}','SearchController@sortDownto');

    //CỬA HÀNG
    Route::get('cua-hang','ShopController@getIndex');

    // PHÂN LOẠI
    Route::get('thoi-trang-nam','ManController@getIndex');
    Route::get('thoi-trang-nam/{category}','ManController@getCategory');

    Route::get('thoi-trang-nu','WomanController@getIndex');
    Route::get('thoi-trang-nu/{category}','WomanController@getCategory');

    //GIỚI THIỆU
    Route::get('gioi-thieu','ProduceController@getIndex');

    //CHI TIẾT SẢN PHẨM
    Route::get('chi-tiet/{slug}','ProduceController@getDetail');
    //BÌNH LUẬN
    Route::post('comment/{id}','ProduceController@postComment');

    //GIỎ HÀNG
    Route::get('gio-hang','CartController@getCart');
    Route::post('mua-hang/{id}','CartController@postToCart');
    Route::get('xoa-san-pham/{id}','CartController@deleteCart');
    Route::post('thanh-toan','CartController@pay');

    //TÀI KHOẢN
    Route::get('doi-mat-khau','UserController@changePass');
    Route::get('profile/{id}','UserController@profile');
    Route::post('post-profile/{id}','UserController@postProfile');
    Route::post('post-changepass/{id}','UserController@postChangePass');
    Route::get('lich-su-mua-hang/{id}','UserController@getListOder');
    Route::get('chi-tiet-don-hang/{id}','UserController@getListOderDetail');
    Route::get('huy-don-hang/{id}','UserController@cancelOder');
    Route::get('quen-mat-khau','UserController@getForgotPass');
    Route::post('post-forgotpass','UserController@postForgotPass');
    Route::post('checkcode','UserController@ckeckCode');
    
//    Route::get('/','HomeController@getHome');
});


// Login

Route::group(['prefix'=> 'login'], function() {
    Route::get('/','LoginController@getLogin');
    Route::post('/','LoginController@postLogin');
});
Route::get('logout','LoginController@LogOut');
Route::post('signup','LoginController@signUp');

Route::group(['namespace' => 'admin','prefix' => 'admin','middleware' => 'CheckLogin','middleware' => 'CheckGuest'], function() {



    Route::get('/','DashboardController@getDashboard');
    Route::post('/','DashboardController@getDashboard');

    //LOẠI SẢN PHẨM

    Route::group(['prefix' => 'category','middleware' => 'CheckLevel'], function() {
        Route::get('list','CategoryController@getList');
        Route::post('postadd','CategoryController@postAdd');
        Route::get('delete/{name}','CategoryController@delete');
        Route::post('edit/{name}','CategoryController@edit');

    });

    // SẢN PHẨM
	Route::group(['prefix' => 'product'], function() {

        Route::get('list/{id}','ProductController@getList');
        Route::get('add/{id}','ProductController@getAdd');
        Route::post('postadd/{id}','ProductController@postAdd');
        Route::get('edit/{id}','ProductController@getEdit');
        Route::post('postedit/{id}','ProductController@postEdit');
        Route::get('delete/{id}','ProductController@delete');
	});

    // NGƯỜI DÙNG
    Route::group(['prefix' => 'user','middleware' => 'CheckLevel'], function() {
        Route::get('/','UserController@getList');
        Route::get('add','UserController@getAdd');
        Route::post('postadd','UserController@postAdd');
        Route::get('edit','UserController@getEdit');
        Route::get('block/{id}','UserController@block');
        Route::get('unblock/{id}','UserController@unblock');
    });
    Route::get('user/profile','UserController@getProfile');
    Route::post('user/post-profile','UserController@postProfile');
    Route::get('user/doi-mat-khau','UserController@getChangepass');
    Route::post('user/post-changepass','UserController@postChangePass');
    //Route::post('user/post-profile','UserController@postProfile');

    // ĐƠN HÀNG
    Route::group(['prefix' => 'order'], function() {
        Route::get('list','OderController@getList');
        Route::get('order-detail/{id}','OderController@getDetail');
        Route::get('status/{id}','OderController@updateOrder');
        Route::get('giao-hang/{id}','OderController@updateOrder2');
        Route::get('delete/{id}','OderController@delete');
    });

    //Banner
    Route::group(['prefix' => 'banner'], function() {
        Route::get('list','BannerController@getList');
        Route::get('add','BannerController@getAdd');
        Route::post('postadd','BannerController@postAdd');
        Route::get('edit/{id}','BannerController@getEdit');
        Route::post('postedit/{id}','BannerController@postEdit');
        Route::get('delete/{id}','BannerController@delete');
    });

});
