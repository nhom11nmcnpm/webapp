<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productsize', function (Blueprint $table) {

            $table->increments('size_id');
            
            $table->integer('quantity');
            $table->integer('size');
            

            $table->integer('product_id')->unsigned();
            
            $table->foreign('product_id')
            ->references('product_id')
            ->on('products')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productsize');
    }
}
