<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_size extends Model
{
    protected $table = 'productsize';
    protected $primaryKey = 'size_id';
    
}
