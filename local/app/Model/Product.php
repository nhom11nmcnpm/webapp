<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'product_id';
    public function phanloai()
    {
        return $this->belongsTo('App\Model\Category', 'category_id');
    }

    public function getSize()
    {
        return $this->hasMany('App\Model\Product_size', 'product_id');
    }

}
