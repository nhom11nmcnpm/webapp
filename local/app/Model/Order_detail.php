<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    protected $table = 'order_details';
    protected $primaryKey = 'order_detail_id';
    public function oderDetail()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }
    public function oderProduct()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }
}
