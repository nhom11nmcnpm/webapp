<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'comment_id';
    public function userComment()
    {
        return $this->belongsTo('App\Model\User', 'id');
    }
    public function productComment()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }
}
