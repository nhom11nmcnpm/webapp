<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;
class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'order_id';
    public function userOder()
    {
        return $this->belongsTo('App\Model\User', 'id');
    }
}
