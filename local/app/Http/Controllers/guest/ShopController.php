<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
class ShopController extends Controller
{
    public function getIndex(){
        $data['product'] = Product::orderby('product_id','desc')->paginate(12);
    	return view('guest.shop',$data);
    }
}
