<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Comment;
use App\Model\Product_size;
use Auth;
class ProduceController extends Controller
{
    public function getIndex(){
    	return view('guest.introduce');
    }

    public function getDetail($slug){
        $data['product'] = Product::where('slug',$slug)->first();  
        $data['images'] = explode(",",$data['product']->images);
        $data['comment'] = Comment::where('product_id',$data['product']->product_id)->orderBy('comment_id','desc')->paginate(8);
        $data['size'] = Product_size::where('product_id',$data['product']->product_id)->where('quantity','>',0)->get();
        //dd($data['size']);
        return view('guest.product_detail',$data);
    }

    //BÌNH LUẬN
    public function postComment($id,Request $rq){
        $comment = new Comment;
        $comment->content = $rq->comment;
        $comment->id = Auth::user()->id;
        $comment->product_id = $id;
        $comment->save();
        return back();
    }
}
