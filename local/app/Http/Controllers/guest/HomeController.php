<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use App\Model\Product;
use App\Model\Category;
use DB;
class HomeController extends Controller
{
    public function getHome(){
       
        $data['banner'] = Banner::orderBy('id','desc')->get();
        $data['new_pr'] = Product::orderBy('product_id','desc')->limit(4)->get();
        
        $data['man'] = DB::table('products')->join('categories', 'products.category_id', '=', 'categories.category_id')->where('sex',1)->orderBy('product_id','desc')->limit(2)->get();
        $data['woman'] = DB::table('products')->join('categories', 'products.category_id', '=', 'categories.category_id')->where('sex',0)->orderBy('product_id','desc')->limit(2)->get();
    	return view('guest.home',$data);
    }

}
