<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use DB;
class WomanController extends Controller
{
    public function getIndex(){
        $data['product'] = DB::table('products')->join('categories', 'products.category_id', '=', 'categories.category_id')->where('sex',0)->orderBy('product_id','desc')->paginate(12);
    	return view('guest.woman',$data);
    }

    public function getCategory($category){
        $data['product'] = DB::table('products')->join('categories', 'products.category_id', '=', 'categories.category_id')->where([
            ['sex', '=', 0],
            ['name', '=', $category]
        ])->orderBy('product_id','desc')->paginate(12);
        return view('guest.woman',$data);
    }
}
