<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\User;
use App\Model\Order;
use App\Model\Order_detail;
use App\Model\Product;
use App\Model\Product_size;
use File,Hash,Auth,Session,Mail;
class UserController extends Controller
{
    var $send_mail;
    public function changePass(){
		return view('guest.changepass');
	}
	public function profile($id){
		$data['user'] = User::where('id',$id)->first();

		return view('guest.profile',$data);
    }

    //ĐỔI THÔNG TIN CÁ NHÂN
    public function postProfile($id,Request $rq){
        $user = User::find($id);
        $user->name = $rq->name;
        $user->phone = $rq->phone;
        if(isset($rq->avatar)){
            if($user->avatar != 'default-avatar.jpeg'){  
                File::delete('local/storage/app/public/images/'.$user->avatar);      
            } 
            $filename = $rq->avatar->getClientOriginalName();
            $path = "public/images";
            $filename = time().$filename;
            $rq->avatar->storeAs($path,$filename);
            $user->avatar = $filename; 

            
        }
        $user->save();
        return back()->with('error','Thay đổi thông tin thành công!');
    }

    // ĐỔI MẬT KHẨU
    public function postChangePass($id,Request $rq){
        $user = User::find($id);
        ;
        if(Hash::check($rq->password_old,$user->password)){
            $user->password = bcrypt($rq->password);
            $user->save();
            return back()->with('error','Thay đổi mật khẩu thành công!');
        }
        else{
            return back()->with('error','Mật khẩu cũ không chính xác!');
        }
    }

    //LỊCH SỬ MUA HÀNG
    public function getListOder($id){
        $data['order'] = Order::where('id',$id)->get();
        return view('guest.order',$data);
    }

    //CHI TIẾT ĐƠN HÀNG
    public function getListOderDetail($id){
        $data['order'] = Order::find($id);
        $data['order_detail'] = Order_detail::where('order_id',$id)->get();
        $total_money = 0;
        foreach($data['order_detail'] as $item){
            $total_money+= $item->quantity*$item->oderProduct->price;
        }
        $data['total_money']=$total_money;
        return view('guest.order_detail',$data);
    }

    //HỦY ĐƠN HÀNG
    public function cancelOder($id){
        $order = Order::find($id);
        $order->status='Đã hủy!';
        $order->save();

        $order_detail = Order_detail::where('order_id',$id)->get();
        foreach($order_detail as $item){
            $old_size = Product_size::where('product_id',$item->product_id)->where('size',$item->size)->first();
            $quantity = $old_size->quantity + $item->quantity;
            $update_size = Product_size::where('product_id',$item->product_id)->where('size',$item->size)->update(['quantity' => $quantity]);
        }
        return back();
    }

    // QUÊN MẬT KHẨU
    public function getForgotPass(){
        return view('guest.forgotpass');
    }
    public function postForgotPass(Request $rq){
        $check = null;
        $check = User::where('email',$rq->email)->first();
        $data['email'] = $rq->email;
        if($check == null){
            return back()->with('error','Email chưa được đăng kí!'); 
        }
        else{
            $code = rand();
            Session::put('code', $code);
            Session::put('email', $rq->email);
            $data['code'] = $code;
            $this->send_mail = $rq->email;
            Mail::send('guest.mail',$data,function($message){
                $message->from('leduyanh24071998@gmail.com','Fashionshop');
                $message->to($this->send_mail,'Visitor')->subject('Đổi mật khẩu');
            });
            return view('guest.forgotpass2',$data);
        } 
    }
    public function ckeckCode(Request $rq){
        if(Session::get('code') == $rq->code){
            $pass = bcrypt($rq->password);
            $email = Session::get('email');
            $user = User::where('email',$email)->first();
            $user->password = $pass;
            $user->save();
            return redirect('login')->with('error','Đổi mật khẩu thành công, mời bạn đăng nhập');
        }
        else{
            return redirect('quen-mat-khau')->with('error','Mã xác nhận không đúng!');
        }
    }
}
