<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Product_size;
use App\Model\Order;
use App\Model\Order_detail;
use Cart;
use Auth;
class CartController extends Controller
{
    public function getCart(){
        $data['cart']= Cart::getContent();
    	return view('guest.cart',$data);
    } 

    //THÊM SẢN PHẨM VÀO GIỎ HÀNG
    public function postToCart($id,Request $rq){
       
        $size = 0;
        $quantity=0;
        
        if(isset($rq->quantity_36)){
            $size = 36;
            $quantity=$rq->quantity_36;
        }
        if(isset($rq->quantity_37)){
            $size = 37;
            $quantity=$rq->quantity_37;
        }
        if(isset($rq->quantity_38)){
            $size = 38;
            $quantity=$rq->quantity_38;
        }
        if(isset($rq->quantity_39)){
            $size = 39;
            $quantity=$rq->quantity_39;
        }
        if(isset($rq->quantity_40)){
            $size = 40;
            $quantity=$rq->quantity_40;
        }
        if(isset($rq->quantity_41)){
            $size = 41;
            $quantity=$rq->quantity_41;
        }
        if(isset($rq->quantity_42)){
            $size = 42;
            $quantity=$rq->quantity_42;
        }
        if(isset($rq->quantity_43)){
            $size = 43;
            $quantity=$rq->quantity_43;
        }
    
        if($size ==0 ){
            return back()->with('error','Bạn chưa chọn size!');
        }
        else{
            $product = Product::find($id);
            $id_cart = $id+$size;
            Cart::add(array('id'=>$id_cart,'name'=>$product->pr_name,'price'=>$product->price,'quantity'=>$quantity,'attributes'=>array('avatar'=>$product->avatar,'size'=>$size)));
            return redirect('gio-hang');
        }
        
        
    }
    //END THÊM SẢN PHẨM VÀO GIỎ HÀNG

    //XÓA SẢN PHẨM TRONG GIỎ HÀNG
    public function deleteCart($id){
        Cart::remove($id);
        return back();
    }

    //THANH TOÁN
    public function pay(Request $rq){
        $order_old = Order::orderBy('order_id','desc')->limit(1)->first();
        if($order_old == null){
            $id_old = 0;
        }
        else{
            $id_old = $order_old->order_id;
        }
    
        $order = new Order;
        $order->order_id = $id_old+1;
        $order->id = Auth::user()->id;
        $order->phone = $rq->phone;
        $order->pay = Cart::getTotal();
        $order->email = $rq->email;
        $address = $rq->name.', '.$rq->address;
        $order->address = $address;
        $order->status = 'Đang xử lý';
        $order_id = $order->order_id;
        
        $order->save();
        
        foreach(Cart::getContent() as $item){
            $order_detail = new Order_detail;
            $product_id = $item->id - $item->attributes->size ;

            $old_size = Product_size::where('product_id',$product_id)->where('size',$item->attributes->size)->first();
            $quantity = $old_size->quantity - $item->quantity;
            $update_size = Product_size::where('product_id',$product_id)->where('size',$item->attributes->size)->update(['quantity' => $quantity]);

            $order_detail->product_id = $product_id;
            $order_detail->order_id = $id_old+1;
            $order_detail->quantity = $item->quantity;
            $order_detail->size=$item->attributes->size;
            $order_detail->save();
        }
        Cart::clear();
        return back()->with('error','Quý khách đã thanh toán thành công!');
    }
}
