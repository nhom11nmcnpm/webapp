<?php

namespace App\Http\Controllers\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
class SearchController extends Controller
{
    // TÌM KIẾM CƠ BẢN
    public function search(Request $rq){
        $data['product'] = Product::where('pr_name', 'like', '%'.$rq->search.'%')->get();
        $data['search'] = $rq->search;
        $data['text'] = '';
    	return view('guest.search',$data);
    }

    // SẮP XẾP THEO MỚI NHẤT
    public function searchNewbie($search){

        $data['product'] = Product::where('pr_name', 'like', '%'.$search.'%')->orderBy('product_id','desc')->get();
        $data['search'] = $search;
        $data['text'] = '';
        return view('guest.search',$data);
    }

    // SẮP XẾP TỪ CAO ĐẾN THẤP
    public function sortDownto($search){
        
        $data['product'] = Product::where('pr_name', 'like', '%'.$search.'%')->orderBy('price','desc')->get();
        $data['search'] = $search;
        $data['text'] = 'Cao đến thấp';
        return view('guest.search',$data);
    }

    // SẮP XẾP TỪ THẤP ĐẾN CAO
    public function sortUpto($search){
        $data['product'] = Product::where('pr_name', 'like', '%'.$search.'%')->orderBy('price','asc')->get();
        $data['search'] = $search;
        $data['text'] = 'Thấp đến cao';
        return view('guest.search',$data);
    }
}
