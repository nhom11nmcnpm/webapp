<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Auth,Hash;
class UserController extends Controller
{
    public function getList(){
        $data['user'] = User::orderBy('level','desc')->get();
    	return view('admin.user.list',$data);
    }

    // ADD USER
    public function getAdd(){
    	return view('admin.user.add');
    }
    public function postAdd(Request $rq){
        $user = new User;
        $user->name = $rq->name;
        $user->email = $rq->email;
        $user->phone = $rq->phone;
        $user->level = $rq->level;
        $user->flag = 0;
        $user->password = bcrypt($rq->password);

      if($rq->avatar == ''){
            $filename = 'default-avatar.jpeg';
        }
        else{
            $filename = $rq->avatar->getClientOriginalName();
            $path = "public/images";
            $filename = time().$filename;
            $rq->avatar->storeAs($path,$filename);
        }          
        $user->avatar = $filename;
        $user->save();
        return redirect('admin/user');
    }
    //END ADD USER

    // BLOCK
    public function block($id){
        $user = User::find($id);
        $user->flag = 1;
        $user->save();
        return back();
    }

    // UNBLOCK
    public function unblock($id){
        $user = User::find($id);
        $user->flag = 0;
        $user->save();
        return back();
    }

    public function getEdit(){
    	return view('admin.user.edit');
    }

    //PROFILE
    public function getProfile(){
        $data['user'] = User::where('id',Auth::user()->id)->first();
		return view('admin.user.profile',$data);
    }

    //ĐỔI THÔNG TIN CÁ NHÂN
    public function postProfile(Request $rq){
        $user = User::find(Auth::user()->id);
        $user->name = $rq->name;
        $user->phone = $rq->phone;
        if(isset($rq->avatar)){
            if($user->avatar != 'default-avatar.jpeg'){  
                File::delete('local/storage/app/public/images/'.$user->avatar);      
            } 
            $filename = $rq->avatar->getClientOriginalName();
            $path = "public/images";
            $filename = time().$filename;
            $rq->avatar->storeAs($path,$filename);
            $user->avatar = $filename;             
        }
        $user->save();
        return back()->with('error','Thay đổi thông tin thành công!');
    }

    //ĐỔI MẬT KHẨU
    public function getChangepass(){
		return view('admin.user.changepass');
    }
    
    // ĐỔI MẬT KHẨU
    public function postChangePass(Request $rq){
        $user = User::find(Auth::user()->id);
        ;
        if(Hash::check($rq->password_old,$user->password)){
            $user->password = bcrypt($rq->password);
            $user->save();
            return back()->with('error','Thay đổi mật khẩu thành công!');
        }
        else{
            return back()->with('error','Mật khẩu cũ không chính xác!');
        }
    }
}
