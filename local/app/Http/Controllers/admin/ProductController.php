<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product;
use App\Model\Category;
use App\Model\Comment;
use App\Model\Product_size;
use File;
class ProductController extends Controller
{
    public function getList($id){
        $data['product'] = Product::where('category_id',$id)->orderBy('product_id','desc')->get();
        $data['category'] = Category::where('category_id',$id)->first();
    	return view('admin.product.list',$data);
    }

    public function getAdd($id){
        $data['category'] = $id;
    	return view('admin.product.add',$data);
    }

    public function postAdd($id,Request $rq){
        $pr_max_id = Product::orderBy('product_id','desc')->limit(1)->first();
        if($pr_max_id != null){
            $max_id = $pr_max_id->product_id +1;
        }
        else{
            $max_id = 1;
        }
  
        $product = new Product;
        $product->pr_name = $rq->pr_name;
        $product->description = $rq->description;
        $product->price = $rq->price;
        $product->import_price = $rq->im_price;
        $product->category_id = $id;

        $slug = $rq->pr_name.'-'.$max_id;
        $product->slug = str_slug($slug);

        $images = '';
        if(isset($rq->images)){
            foreach($rq->images as $item){
                $filename = $item->getClientOriginalName();
                $path = "public/images";
                $filename = time().$filename;
                $item->storeAs($path,$filename);
    
                if($images == ''){
                    $images = $filename;
                }
                else{
                    $images = $images.','.$filename;
                }
               
            }
            $product->images = $images;
        }
        

        if($rq->avatar == null){
            $filename = '29541772703_6ed8b50c47_b.jpg';
        }
        else{
            $filename = $rq->avatar->getClientOriginalName();
            $path = "public/images";
            $filename = time().$filename;
            $rq->avatar->storeAs($path,$filename);
        }          
        $product->avatar = $filename;
        $product->save();
        foreach($rq->size as $key=>$item){
            if($item != null && $item != 0){
                $size = new Product_size;
                $size->product_id = $max_id;
                $size->size = $key + 36;
                $size->quantity = $item;
                $size->save();
            }
        }
        return redirect('admin/product/list/'.$id);
    }
    public function getEdit($id){
        $data['product'] = Product::where('product_id',$id)->first();
        $data['size'] = Product_size::where('product_id',$id)->get();
       
    	return view('admin.product.edit',$data);
    }

    public function postEdit($id,Request $rq){
        $product = Product::find($id);
        $product->pr_name = $rq->pr_name;
        $product->description = $rq->description;
        $product->price = $rq->price;
        $product->import_price = $rq->im_price;
        $slug = $rq->pr_name.'-'.$id;
        $product->slug = str_slug($slug);

        if(isset($rq->avatar)){

            $filename = $rq->avatar->getClientOriginalName();
            $path = "public/images";
            $filename = time().$filename;
            $rq->avatar->storeAs($path,$filename);  
            File::delete('local/storage/app/public/images/'.$product->avatar);
        
            $product->avatar = $filename;  
        }
        $product->save();

        foreach($rq->size as $key=>$item){
            if($item != null && $item != 0){
               
                $check_size = Product_size::where('product_id',$id)->where('size',$key+36)->first();
               
                if($check_size == null){
                    $size = new Product_size;
                    $size->product_id = $id;
                    $size->size = $key + 36;
                    $size->quantity = $item;
                    $size->save();
                }
                else{
                    $size = Product_size::find($check_size->size_id);
                    $size->quantity = $item;
                    $size->save();
                }
            
            }
        }

        return redirect('admin/product/list/'.$product->category_id);
    }

    public function delete($id){
        $comment = Comment::where('product_id',$id)->get();
        foreach($comment as $item){
            Comment::destroy($item->comment_id);
        }
        
        $product = Product::find($id);
        File::delete('local/storage/app/public/images/'.$product->avatar); 
        $images = explode(",",$product->images);
        foreach($images as $item){
            File::delete('local/storage/app/public/images/'.$item);
        }
        Product::destroy($id);
        
        return back(); 
    }
}
