<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
class CategoryController extends Controller
{
    public function getList(){
    	$data['category'] = Category::orderBy('category_id','desc')->get();
    	return view('admin.category.list',$data);
    }

    public function postAdd(Request $rq){
    	$category = new Category;
    	$category->name = $rq->name;
    	$category->sex = '1';
        $category->save();

        $category2 = new Category;
    	$category2->name = $rq->name;
    	$category2->sex = '0';
    	$category2->save();

    	return redirect('admin/category/list');

    }
    public function edit($name,Request $rq){
        
        Category::where('name',$name)->update(['name' => $rq->name]);
            
        return redirect('admin/category/list');
    }
    public function delete($name){
    	Category::where('name',$name)->delete();
        return back();
    }
}
	