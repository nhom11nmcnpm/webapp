<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Category;
use App\Model\Comment;
use App\Model\User;
use App\Model\Order;
use App\Model\Order_detail;
use DB;
class DashboardController extends Controller
{
    public function getDashboard(Request $rq){
        $data['product'] = Product::get();
        $data['user'] = User::where('level',1)->get();
        $data['order'] = Order::where('status','Đang xử lý')->get();

        $data['total_product'] = count($data['product']);
        
        $data['total_user'] = count($data['user']);
        $data['total_order'] = count($data['order']);
        $data['date'] = date('Y-m-d');

        $order_pay = Order::where('status','Đã thanh toán')->get();
        $data['revenue'] = 0;
        foreach($order_pay as $item){
            $data['revenue']+= $item->pay; 
        }

        // THỐNG KÊ ĐƠN HÀNG THEO NĂM 
        if(isset($rq->year)){
            $year = $rq->year;
        }
        else{
            $year=2018;
        }
        $order = Order::whereYear('created_at', $year)->get();
        $order_detail = Order_detail::whereYear('created_at', $year)->get();
    
        $data['thong_ke'] = array();
        for($i=1;$i<13;$i++){
            $total_order = 0;
            $total_product = 0;
            $total_revenue = 0;
            $total_interest =0;
            $j=$i+1;
            if($i<10){
                $month = $year.'-0'.$i;
                $month_next = $year.'-0'.$j;
            }
            else{
                $month = $year.'-'.$i;
                $month_next = $year.'-'.$j;
            }

            foreach($order as $item){
                if($item->status == 'Đã thanh toán'){
                    if($item->created_at >= $month && $item->created_at < $month_next){
                        $total_order++;
                        $total_revenue+= $item->pay;
                    }
                }   
                
            }

            foreach($order_detail as $item){    
                
                if($item->oderDetail->status == 'Đã thanh toán'){ 
                    if($item->created_at >= $month && $item->created_at < $month_next){
                        $total_product++;
                        $total_interest+= $item->oderProduct->price - $item->oderProduct->import_price;
                    }
                }
            }
            $tmp =array('so_don_hang' => $total_order,
                        'so_san_pham'=>$total_product,
                        'doanh_thu' => $total_revenue,
                        'lai_suat' => $total_interest);
            array_push($data['thong_ke'],$tmp);
        }
        $data['year'] = $year;
        //HẾT PHẦN THỐNG KÊ THEO NĂM
       
    	return view('admin.index',$data);
    }
}
