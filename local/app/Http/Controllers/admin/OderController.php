<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\Order_detail;
class OderController extends Controller
{
    public function getList(){
        $data['order']= Order::orderBy('order_id','desc')->get();
    	return view('admin.order.list',$data);
    }

    //CHI TIẾT ĐƠN HÀNG
    public function getDetail($id){
        $data['order_detail'] =  Order_detail::where('order_id',$id)->get();
        $data['order']= Order::where('order_id',$id)->first();
        $total_money = 0;
        foreach($data['order_detail'] as $item){
            $total_money+= $item->quantity*$item->oderProduct->price;
        }
        $data['total_money']=$total_money;
        
        return view('admin.order.show',$data);
    }

    // CẬP NHẬT TRANG THÁI

    public function updateOrder2($id){
        $order = Order::find($id);
        $order->status='Đang giao hàng';
        $order->save();
        return back();
    }
    public function updateOrder($id){
        $order = Order::find($id);
        $order->status='Đã thanh toán';
        $order->save();
        return back();
    }

    //XÓA ĐƠN HÀNG 
    public function delete($id){
        Order::where('order_id',$id)->delete();
        return back();
    }

}
