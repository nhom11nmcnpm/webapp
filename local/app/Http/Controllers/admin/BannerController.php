<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Banner;
use File;
class BannerController extends Controller
{
    public function getList(){
      $data['banner'] = Banner::orderBy('id','desc')->get();

      return view('admin.banner.list',$data);
    }
    public function getAdd(){
      return view('admin.banner.add');
    }
    public function getEdit($id){
      $data['banner'] = Banner::where('id',$id)->first();
      return view('admin.banner.edit',$data);
    }

    public function postAdd(Request $rq){
      $banner = new Banner;
      $banner->title = $rq->title;
      $banner->description = $rq->description;

      if($rq->images == ''){
            $filename = '29541772703_6ed8b50c47_b.jpg';
        }
        else{
            $filename = $rq->images->getClientOriginalName();
            $path = "public/images";
            $filename = time().$filename;
            $rq->images->storeAs($path,$filename);
        }          
        $banner->images = $filename;
        $banner->save();
        return redirect('admin/banner/list');
      
    }
    public function postEdit(Request $rq,$id){
      $banner = Banner::find($id);
      $banner->title = $rq->title;
      $banner->description = $rq->description;

      if(isset($rq->images)){

        $filename = $rq->images->getClientOriginalName();
        $path = "public/images";
        $filename = time().$filename;
        $rq->images->storeAs($path,$filename);  
        File::delete('local/storage/app/public/images/'.$banner->images);
     
        $banner->images = $filename;  
      }
      $banner->save();

      return redirect('admin/banner/list');
      
    }
    public function delete($id){
      $banner = Banner::find($id);
        File::delete('local/storage/app/public/images/'.$banner->images); 
        Banner::destroy($id);
        return back(); 
    }
}
