<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\User;
class LoginController extends Controller
{
    public function getLogin(){
        if(Auth::check()){
			if(Auth::user()->level == 1){
				return redirect('/');
			}
			else{
				return redirect('admin');
			}
    	}
    	return view('guest.login');
    }

    public function postLogin(Request $request){
        
		$info = 
		[
			'email' => $request->email, 
			'password' =>$request->password
		];
		if( Auth::attempt($info, true) ) {
			if(Auth::check() && Auth::user()->flag == 0){
				return redirect('admin');
			}
			else{
				Auth::logout();
				return back()->with('error','Tài khoản của bạn đã bị khóa!');
			}
		} 
		else {
			return back()->with('error','Tài khoản hoặc mật khẩu không đúng!');
		}
	}
	public function LogOut(){
		Auth::logout();
		return redirect('/');
	}

	

	public function signUp(Request $rq){
		$check_user = User::where('email',$rq->email)->first();
		if($check_user != null){
			return back()->with('error','Email này đã được sử dụng!');
		}
		$user = new User;
        $user->name = $rq->name;
        $user->email = $rq->email;
        $user->level = 1;
		$user->flag = 0;
		$user->avatar = 'default-avatar.jpeg';
        $user->password = bcrypt($rq->password);

		$user->save();
		
        return back()->with('error','Đăng kí thành công, mời bạn đăng nhập!');
	}
}
